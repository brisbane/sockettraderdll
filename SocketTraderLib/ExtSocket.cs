﻿using System;
using System.Collections.Concurrent;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using RGiesecke.DllExport;

namespace SocketTrader {
	public static class ExtSocket {
		private static ConcurrentDictionary<int, SocketClient> dict;
		private static int lastId;
		private static string lastError;

		static ExtSocket() {
			try {
				dict = new ConcurrentDictionary<int, SocketClient>();
			} catch (Exception e) {
				Error(e);
			}
		}

		[DllExport(CallingConvention = CallingConvention.StdCall)]
		public static int SC_CreateSocket() {
			try {
				SocketClient socketClient = new SocketClient();
				int id = Interlocked.Increment(ref lastId);
				if (dict.TryAdd(id, socketClient)) {
					return id;
				}
			} catch (Exception e) {
				Error(e);
			}
			return -1;
		}

		[DllExport(CallingConvention = CallingConvention.StdCall)]
		public static void SC_DestroySocket(int id) {
			SocketClient socketClient = getSocketClient(id);
			if (socketClient != null) {
				socketClient.Disconnect();
				dict.TryRemove(id, out socketClient);
			}
		}

		private static SocketClient getSocketClient(int id) {
			SocketClient socketClient;
			if (dict.TryGetValue(id, out socketClient)) {
				return socketClient;
			}
			Error("not found socket with id = " + id);
			return null;
		}

		[DllExport(CallingConvention = CallingConvention.StdCall)]
		public static bool SC_Connect(int id, [MarshalAs(UnmanagedType.LPWStr)] string address, int port) {
			SocketClient socketClient = getSocketClient(id);
			if (socketClient != null) {
				return socketClient.Connect(address, port);
			}
			return false;
		}

		[DllExport(CallingConvention = CallingConvention.StdCall)]
		public static void SC_Disconnect(int id) {
			SocketClient socketClient = getSocketClient(id);
			if (socketClient != null) {
				socketClient.Disconnect();
			}
		}

		[DllExport(CallingConvention = CallingConvention.StdCall)]
		public static bool SC_IsConnected(int id) {
			SocketClient socketClient = getSocketClient(id);
			if (socketClient != null) {
				return socketClient.IsConnected();
			}
			return false;
		}

		private static void Error(Exception e) {
			Error(e.ToString());
		}

		private static void Error(string message) {
			lastError = message;
		}

		[DllExport(CallingConvention = CallingConvention.StdCall)]
		public static int SC_GetLastErrorString(int id,
			[In, Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 2)] byte[] array, int arraySize) {
			SocketClient socketClient = getSocketClient(id);
			if (socketClient != null) {
				return socketClient.GetLastErrorString(array, arraySize);
			}
			return 0;
		}

		[DllExport(CallingConvention = CallingConvention.StdCall)]
		public static bool SC_Send(int id, [MarshalAs(UnmanagedType.LPWStr)] string message) {
			SocketClient socketClient = getSocketClient(id);
			if (socketClient != null) {
				return socketClient.Send(message);
			}
			return false;
		}

		[DllExport(CallingConvention = CallingConvention.StdCall)]
		public static int SC_Poll(int id, [In, Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 2)] byte[] array,
			int arraySize) {
			SocketClient socketClient = getSocketClient(id);
			if (socketClient != null) {
				return socketClient.Poll(array, arraySize);
			}
			return 0;
		}



		[DllExport(CallingConvention = CallingConvention.StdCall)]
		public static int S_GetLastErrorString([In, Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] byte[] array,
			int arraySize) {
			string ret = lastError;
			lastError = null;
			if (ret == null) return 0;
			byte[] bytes = Encoding.UTF8.GetBytes(ret);
			int length = Math.Min(arraySize, bytes.Length);
			Array.Copy(bytes, 0, array, 0, length);
			return length;
		}
	}
}