﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SocketTrader {
	public class SocketClient {
		private const int MAX_FRAME_BODY_SIZE = 65535;
		private const int FRAME_HEAD_SIZE = 2;

		private string lastError;
		private Socket socket;
		private byte[] sizeBuf = new byte[FRAME_HEAD_SIZE];
		private byte[] buf = new byte[MAX_FRAME_BODY_SIZE];
		private int framePosition;
		private int frameSize;

		private enum State {
			ReadHead,
			ReadBody
		}

		private static State state = State.ReadHead;

		public bool Connect(string address, int port) {
			try {
				IPAddress ipAddress = IPAddress.Parse(address);
				IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
				socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				socket.Connect(remoteEP);
				return true;
			} catch (Exception e) {
				Error(e);
				return false;
			}
		}

		public void Disconnect() {
			if (socket != null) {
				try {
					socket.Disconnect(false);
				} catch (Exception e) {
					Error(e);
				} finally {
					socket = null;
				}
			}
		}

		public bool IsConnected() {
			if (socket == null) {
				return false;
			}
			return !((socket.Poll(1000, SelectMode.SelectRead) && (socket.Available == 0)) || !socket.Connected);
		}

		private void Error(Exception e) {
			Error(e.ToString());
		}

		private void Error(string message) {
			lastError = message;
		}

		public int GetLastErrorString(byte[] array, int arraySize) {
			string ret = lastError;
			lastError = null;
			if (ret == null) return 0;
			byte[] bytes = Encoding.UTF8.GetBytes(ret);
			int length = Math.Min(arraySize, bytes.Length);
			Array.Copy(bytes, 0, array, 0, length);
			return length;
		}

		public bool Send(string message) {
			try {
				var buffer = Encoding.UTF8.GetBytes(message);
				var length = buffer.Length;
				if (length > MAX_FRAME_BODY_SIZE) {
					Error(string.Format("Too long sending message. Max size = {0}, sending siae = {1}", MAX_FRAME_BODY_SIZE, length));
					return false;
				}
				sizeBuf[0] = (byte) (length);
				sizeBuf[1] = (byte) (length >> 8);
				socket.Send(sizeBuf);
				socket.Send(buffer);
				return true;
			} catch (Exception e) {
				Error(e);
				return false;
			}
		}

		public int Poll(byte[] array, int arraySize) {
			try {
				if (state == State.ReadHead) {
					if (socket.Available >= FRAME_HEAD_SIZE) {
						socket.Receive(buf, 0, FRAME_HEAD_SIZE, SocketFlags.None);
						frameSize = BitConverter.ToUInt16(buf, 0);
						state = State.ReadBody;
					}
				}
				if (state == State.ReadBody) {
					int available = socket.Available;
					if (available > 0) {
						int size = Math.Min(available, frameSize - framePosition);
						socket.Receive(buf, framePosition, size, SocketFlags.None);
						framePosition += size;
						if (framePosition == frameSize) {
							Array.Copy(buf, 0, array, 0, frameSize);
							int ret = frameSize;
							//string message = Encoding.UTF8.GetString(buf, 0, frameSize);
							frameSize = 0;
							framePosition = 0;
							state = State.ReadHead;
							return ret;
						}
					}
				}
			} catch (Exception e) {
				Error(e);
			}
			return 0;
		}
	}
}