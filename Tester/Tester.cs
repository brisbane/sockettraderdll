﻿using System;
using System.Text;
using System.Threading;
//using Console = System.Diagnostics.Debug;
using SocketTrader;


namespace Tester {
	internal class Tester {
		private static void Main(string[] args) {
			byte[] array = new byte[65535];
			int id = ExtSocket.SC_CreateSocket();
			var connect = ExtSocket.SC_Connect(id, "127.0.0.1", 7777);
			Console.WriteLine(connect);
			if (ExtSocket.SC_IsConnected(id)) {
				Console.WriteLine(ExtSocket.SC_Send(id, "f;Hello"));
				while (true) {
					int readCount = ExtSocket.SC_Poll(id, array, array.Length);
					string poll = Encoding.UTF8.GetString(array, 0, readCount);
					if (poll != null) {
						Console.WriteLine(poll);
						if (poll == "quit") {
							break;
						}
					}

					Thread.Sleep(200);
				}
			} else {
				int readCount = ExtSocket.SC_GetLastErrorString(id, array, array.Length);
				string message = Encoding.UTF8.GetString(array, 0, readCount);
				Console.WriteLine(message);
			}
			Console.WriteLine("done");
			Console.ReadKey();
		}
	}
}